using System;
using _420_14C_FX.Classes;
using Newtonsoft.Json.Bson;
using Xunit;

namespace Tests
{
    public class PartieTests
    {

        public Partie InstancierUnePartieAvec2Joueurs()
        {
            Partie partie = new Partie();
            partie.Joueurs.AjouterDebut(new Joueur("joueur1"));
            partie.Joueurs.AjouterDebut(new Joueur("joueur2"));
            return partie;
        }

        #region DemarrerPartie

        [Fact]
        public void Constructeur_Partie_Devrait_Instancier_UneListeDeJoueurs_Encore_Vide()
        {
            //Arrange
            Partie partie = new Partie();

            //Act
            partie.DemarrerPartie();

            //Assert
            Assert.NotNull(partie.Joueurs);
            Assert.Equal(0, partie.Joueurs.Taile);
        }


        [Fact]
        public void DemarrerPartie_Devrait_Retourner_Faux_Si_Nombre_Joueurs_Moins_Que2()
        {
            //Arrange
            Partie partie = new Partie();
            Joueur joueur = new Joueur("joueur1");
            partie.Joueurs.AjouterDebut(joueur);

            //Act
            bool reponseDemarrerPartie = partie.DemarrerPartie();

            //Assert
            Assert.False(reponseDemarrerPartie);
        }

        [Fact]
        public void DemarrerPartie_Devrait_Retourner_Vrai_Quand_Nombre_Joueurs_Plus2()
        {
            //Arrange
            Partie partie = InstancierUnePartieAvec2Joueurs();
            //Act
            bool reponse = partie.DemarrerPartie();

            //Assert
            Assert.True(reponse);
        }

        [Fact]
        public void DemarrerPartie_Devrait_Determiner_JoueurCourant()
        {
            //Arrange
            Partie partie = InstancierUnePartieAvec2Joueurs();

            //Act
            partie.DemarrerPartie();

            //Assert
            Assert.Equal(partie.Joueurs.Debut, partie.JoueurCourant);
        }

        [Fact]
        public void DemarrerPartie_Devrait_Vider_LesMains_De_Tous_LesJoueurs()
        {
            //Arrange
            Partie partie = InstancierUnePartieAvec2Joueurs();

            //Act et Assert
            Noeud joueur = partie.Joueurs.Debut;
            for (int i = 0; i < partie.Joueurs.Taile; i++)
            {
                Assert.Empty(joueur.Valeur.Main);
                joueur = joueur.Suivant;
            }

        }

        [Fact]
        public void DemarrerPartie_Devrait_Instancier_Une_Nouvelle_Pioche()
        {
            //Arrange
            Partie partie = InstancierUnePartieAvec2Joueurs();

            //Act
            partie.DemarrerPartie();

            // Assert
            Assert.NotNull(partie.Pioche);
        }

        [Fact]
        public void DemarrerPArtie_Devrait_Appeler_DistribuerCartes_Et_Ajouter_7cartes_A_Chaque_Joueur()
        {
            //Arrange
            Partie partie = InstancierUnePartieAvec2Joueurs();


            //Act
            partie.DemarrerPartie();

            //Assert
            Noeud joueur = partie.JoueurCourant;
            for (int i = 0; i < partie.Joueurs.Taile; i++)
            {
                Assert.Equal(7, joueur.Valeur.Main.Count);
                joueur = joueur.Suivant;
            }
        }

        [Fact]
        public void DemarrerPartie_Devrait_Appeler_InitialiserDefausse_Et_Instancier_Une_Defausse()
        {
            //Arrange
            Partie partie = InstancierUnePartieAvec2Joueurs();

            //Act
            partie.DemarrerPartie();

            //Assert
            Assert.NotNull(partie.Defausse);

        }

        [Fact]
        public void DemarrerPartie_Devrait_Appeler_InitialiserDefausse_Et_Lui_Ajouter_Une_Seule_Carte_Au_Depart()
        {

            //Arrange
            Partie partie = InstancierUnePartieAvec2Joueurs();

            //Act
            partie.DemarrerPartie();

            //Assert
            Assert.Single(partie.Defausse);
        }

        [Fact]
        public void DemarrerPartie_Devrait_Appeler_InitialiserDefausse_Et_Ajouter_Une_Carte_Valide()
        {
            //Arrange
            Partie partie = InstancierUnePartieAvec2Joueurs();

            //Act
            partie.DemarrerPartie();

            //Assert
            Assert.True((int)partie.Defausse.Pop().Valeur <= 9);
        }



        #endregion


        #region PigerCartes

        [Fact]
        public void PigerCartes_Devrait_Ajouter_Le_Nombre_De_Carte_Dans_La_Main_Du_joueur()
        {

            //Arrange
            Partie partie = InstancierUnePartieAvec2Joueurs();
            Joueur joueur = new Joueur("Joueur3");
            partie.Joueurs.AjouterDebut(joueur);

            partie.DemarrerPartie();

            int nbCartesAPiger = 2;
            int nbDeCartesAttendu = joueur.Main.Count + nbCartesAPiger;


            //Act
            partie.PigerCartes(joueur, nbCartesAPiger);

            //Assert
            Assert.Equal(nbDeCartesAttendu, joueur.Main.Count);
        }


        [Fact]
        public void
            PigerCartes_Devrait_Ajouter_LesCartes_Dans_LaMainJoueur_Meme_Quand_NbCartes_Superieur_Au_Nombre_De_Cartes_Dans_Pioche()
        {

            //Arrange
            Partie partie = InstancierUnePartieAvec2Joueurs();
            Joueur joueur = new Joueur("Joueur3");
            partie.Joueurs.AjouterDebut(joueur);

            partie.DemarrerPartie();

            //Ajouter a la defausse quelques cartes
            for (int i = 0; i < 15; i++)
                partie.Defausse.Push(partie.Pioche.Cartes.Pop());


            int nbCartesAPiger = partie.Pioche.Cartes.Count + 2;
            int nbDeCartesAttendu = joueur.Main.Count + nbCartesAPiger;


            //Act
            partie.PigerCartes(joueur, nbCartesAPiger);

            //Assert
            Assert.Equal(nbDeCartesAttendu, joueur.Main.Count);

        }


        #endregion


        #region JouerCarte

        [Fact]
        public void JouerCarte_Devrait_Lancer_InvalidOperationException_Si_Carte_Invalide()
        {
            //Arrange
            Partie partie = InstancierUnePartieAvec2Joueurs();
            Joueur joueur = new Joueur("Joueur3");

            partie.Joueurs.AjouterDebut(joueur);
            partie.DemarrerPartie();

            Carte carteDefausse = partie.Defausse.Pop();
            Carte carteJoueur = new Carte((Couleur)((int)carteDefausse.Couleur + 1), Valeur.Deux, 2);

            //Act et Assert
            Assert.Throws<InvalidOperationException>(() =>
                partie.JouerCarte(joueur, carteJoueur));
        }

        [Fact]
        public void JouerCarte_Devrait_Lancer_InvalidOperationException_Si_Joueur_NaPas_Crie_Uno()
        {

            //Arrange
            Partie partie = InstancierUnePartieAvec2Joueurs();
            Joueur joueur = new Joueur("Joueur3");
            joueur.Uno = false;

            partie.Joueurs.AjouterDebut(joueur);
            partie.DemarrerPartie();

            Carte carteDefausse = partie.Defausse.Pop();
            Carte carteJoueur = new Carte(carteDefausse.Couleur, Valeur.Deux, 2);

            //Act et Assert
            Assert.Throws<InvalidOperationException>(() =>
                partie.JouerCarte(joueur, carteJoueur));
        }


        [Fact]
        public void JouerCarte_Devrait_Retirer_La_Carte_De_La_Main_Du_Joueur()
        {
            //Arrange
            Partie partie = InstancierUnePartieAvec2Joueurs();

            Joueur joueur = new Joueur("joueur3");
            partie.Joueurs.AjouterDebut(joueur);

            partie.DemarrerPartie();

            int nbCarteInitial = joueur.Main.Count;

            Carte carteJoueur = new Carte(Couleur.Noir, Valeur.Deux, 2);
            joueur.Main.Add(carteJoueur);



            //Act
            partie.JouerCarte(joueur, carteJoueur);

            //Assert
            Assert.Equal(nbCarteInitial, joueur.Main.Count);




        }

        [Fact]
        public void JouerCarte_Devrait_Augmenter_Le_Nombre_De_Cartes_Dans_La_Defausse()
        {
            //Arrange
            Partie partie = InstancierUnePartieAvec2Joueurs();
            partie.DemarrerPartie();

            Carte carte = new Carte(Couleur.Noir, Valeur.Cinq, 5);

            int nbCarteDefausseAttendu = partie.Defausse.Count + 1;

            //Act
            partie.JouerCarte(partie.JoueurCourant.Valeur, carte);

            //Assert
            Assert.Equal(nbCarteDefausseAttendu, partie.Defausse.Count);
        }

        [Fact]
        public void JouerCarte_Devrait_Ajouter_2cartes_Au_Joueur_Suivant_Si_Carte_Jouee_Est_Plus2()
        {

            //Arrange 
            Partie partie = InstancierUnePartieAvec2Joueurs();
            partie.DemarrerPartie();
            //L'emploi de la couleur noire est pour assurer la validation de la carte.
            Carte carte = new Carte(Couleur.Noir, Valeur.Plus2, 20);
            int nbDeCarteAttenduDansLaMainJoeurSuivant = partie.JoueurCourant.Suivant.Valeur.Main.Count + 2;

            //Act
            partie.JouerCarte(partie.JoueurCourant.Valeur, carte);

            //Assert
            Assert.Equal(nbDeCarteAttenduDansLaMainJoeurSuivant, partie.JoueurCourant.Valeur.Main.Count);
        }

        [Fact]
        public void JouerCarte_Devrait_Ajouter_4cartes_Au_Joueur_Suivant_Si_Carte_Jouee_Est_Plus4()
        {

            //Arrange 
            Partie partie = InstancierUnePartieAvec2Joueurs();
            partie.DemarrerPartie();
            //L'emploi de la couleur noire est pour assurer la validation de la carte.
            Carte carte = new Carte(Couleur.Noir, Valeur.Plus4, 20);
            int nbDeCarteAttenduDansLaMainJoeurSuivant = partie.JoueurCourant.Suivant.Valeur.Main.Count + 4;

            //Act
            partie.JouerCarte(partie.JoueurCourant.Valeur, carte);

            //Assert
            Assert.Equal(nbDeCarteAttenduDansLaMainJoeurSuivant, partie.JoueurCourant.Valeur.Main.Count);
        }

        [Fact]
        public void JouerCarte_Devrait_Inverser_Le_Sens_Du_Jeu_Si_La_Carte_Jouee_Est_InverserSens()
        {

            //Arrange 
            Partie partie = InstancierUnePartieAvec2Joueurs();
            partie.Joueurs.AjouterDebut(new Joueur("Joueur3"));
            partie.DemarrerPartie();

            //L'emploi de la couleur noire est pour assurer la validation de la carte.
            Carte carte = new Carte(Couleur.Noir, Valeur.InverserSens, 20);

            //Joueur attendu
            Joueur joueurAttendu = partie.JoueurCourant.Precedent.Valeur;

            //Act
            partie.JouerCarte(partie.JoueurCourant.Valeur, carte);

            //Assert
            Assert.Equal(joueurAttendu, partie.JoueurCourant.Valeur);

        }

        [Fact]
        public void JouerCarte_Devrait_Sauter_Le_Joueur_Suivant_Si_Carte_SauterTour_Est_Jouee()
        {
            //Arrange 
            Partie partie = InstancierUnePartieAvec2Joueurs();
            partie.DemarrerPartie();

            //L'emploi de la couleur noire est pour assurer la validation de la carte.
            Carte carte = new Carte(Couleur.Noir, Valeur.SauterTour, 20);

            //Joueur attendu
            Joueur joueurAttendu = partie.JoueurCourant.Suivant.Suivant.Valeur;

            //Act
            partie.JouerCarte(partie.JoueurCourant.Valeur, carte);

            //Assert
            Assert.Equal(joueurAttendu, partie.JoueurCourant.Valeur);
        }

        #endregion


        #region JoueurSuivant

        [Fact]
        public void JoueurSuivant_Devrait_Lancer_InvalidOperationException_Quand_Nombre_Joueurs_MoinsQue2()
        {
            //Arrange
            Partie partie = new Partie();
            partie.Joueurs.AjouterDebut(new Joueur("Joueur1"));

            //Act et Assert
            Assert.Throws<InvalidOperationException>(() => partie.JoueurSuivant());
        }



        #endregion


    }




}






