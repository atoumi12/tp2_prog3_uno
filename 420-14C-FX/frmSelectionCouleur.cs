﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _420_14C_FX.Classes;

namespace _420_14C_FX
{
    public partial class frmSelectionCouleur : Form
    {
        private const int LARGEUR = 90;
        private const int HAUTEUR = 90;

        private Couleur _couleurSelectionnee;

        public Couleur CouleurSelectionnee
        {
            get { return _couleurSelectionnee; }
            private set { _couleurSelectionnee = value; }
        }

        public frmSelectionCouleur()
        {
            InitializeComponent();
        }

        private void frmSelectionCouleur_Load(object sender, EventArgs e)
        {
            Width = frmSelectionCouleur.LARGEUR * 3;
            Height = frmSelectionCouleur.HAUTEUR * 3;

            pboBleu.Width = frmSelectionCouleur.LARGEUR;
            pboBleu.Height = frmSelectionCouleur.HAUTEUR;
            pboBleu.Location = new Point((Width / 2) - frmSelectionCouleur.LARGEUR -10, 25);
            pboBleu.Tag = Couleur.Bleu;
            

            pboVert.Width = frmSelectionCouleur.LARGEUR;
            pboVert.Height = frmSelectionCouleur.HAUTEUR;
            pboVert.Location = new Point(pboBleu.Location.X + pboBleu.Width , pboBleu.Location.Y);
            pboVert.Tag = Couleur.Vert;

            pboJaune.Width = frmSelectionCouleur.LARGEUR;
            pboJaune.Height = frmSelectionCouleur.HAUTEUR;
            pboJaune.Location = new Point(pboVert.Location.X, pboVert.Location.Y + pboVert.Height );
            pboJaune.Tag = Couleur.Jaune;

            pboRouge.Width = frmSelectionCouleur.LARGEUR;
            pboRouge.Height = frmSelectionCouleur.HAUTEUR;
            pboRouge.Location = new Point(pboBleu.Location.X, pboBleu.Location.Y + pboBleu.Height);
            pboRouge.Tag = Couleur.Rouge;

          
        }


        private void PictureBox_Click(object sender, EventArgs e)
        {
            PictureBox pboCouleur = (PictureBox)sender;
            CouleurSelectionnee = (Couleur) (pboCouleur.Tag);

            DialogResult = DialogResult.OK;
        }

       
    }
}
