﻿#region MÉTADONNÉES

// Nom du fichier : frmAuthentification.cs
// Auteur : Ahmed Toumi (ahmed)
// Date de création : 2021-04-02
// Date de modification : 2021-04-06

#endregion

#region USING

using System;
using System.Windows.Forms;
using _420_14C_FX.Classes;

#endregion

namespace _420_14C_FX
{
    
    public partial class frmAuthentification : Form
    {
        #region ATTRIBUTS

        private string _nomUtilisateur;
        private string _motPasse;
        private string _serveur;
        private int _port;

        #endregion

        #region PROPRIÉTÉS ET INDEXEURS

        public string NomUtilisateur
        {
            get { return _nomUtilisateur; }
            set { _nomUtilisateur = value; }
        }

        public string MotPasse
        {
            get { return _motPasse; }
            set { _motPasse = value; }
        }

        public string Serveur
        {
            get { return _serveur; }
            set { _serveur = value; }
        }

        public int Port
        {
            get { return _port; }
            set { _port = value; }
        }

        #endregion

        #region CONSTRUCTEURS

        public frmAuthentification()
        {
            InitializeComponent();
        }

        #endregion

        #region MÉTHODES

        private void frmClient_Load(object sender, EventArgs e)
        {
            txtAdresseServeur.Text = Utilitaire.ObtenirAdresseIpLocale();
            txtPort.Text = 100.ToString();
        }


        /// <summary>
        /// Permet de valider les informations saisies par l’utilisateur avant la mise à jour des
        /// propriétés du formulaire suivantes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConnecter_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValiderUtilisateur())
                {
                    Serveur = txtAdresseServeur.Text;
                    Port = int.Parse(txtPort.Text);
                    NomUtilisateur = txtNomUtilisateur.Text.ToLower();
                    MotPasse = txtMotPasse.Text;


                    DialogResult = DialogResult.OK;
                }
                else
                {
                    // Retourne rien
                    DialogResult = DialogResult.None;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("une erreur s'est produite : \n" + ex.Message);
            }
        }


        /// <summary>
        /// Permet de valider le formulaire 
        /// </summary>
        /// <returns>true si valide sinon false</returns>
        private bool ValiderUtilisateur()
        {
            //s'assurer de le vider à chauqe validation
            errorProvider.Clear();

            string messages = "";

            //Validation du nom
            if (string.IsNullOrWhiteSpace(txtNomUtilisateur.Text))
                errorProvider.SetError(txtNomUtilisateur, "Veuillez saisir un nom d'utilisateur");

            // On verifie si le nom respecte le nombre de caraceteres indiques
            else if (txtNomUtilisateur.Text.Trim().Length < JetonAuthentification.NB_MIN_CARACTERES_NOM)
                errorProvider.SetError(txtNomUtilisateur,
                    $"Le nom doit être contenir au moins {JetonAuthentification.NB_MIN_CARACTERES_NOM} caractères");

            //Validation mot de passe
            if (string.IsNullOrWhiteSpace(txtMotPasse.Text))
                errorProvider.SetError(txtMotPasse, "Veuillez saisir un mot de passe de l'utilisateur");

            // On verifie si le nom respecte le nombre de caraceteres indiques
            else if (txtMotPasse.Text.Trim().Length < JetonAuthentification.NB_MIN_CARACTERES_MDP)
                errorProvider.SetError(txtMotPasse,
                    $"Le mot de passe doit être contenir au moins {JetonAuthentification.NB_MIN_CARACTERES_MDP} caractères");


            // Validation du serveur
            if (string.IsNullOrWhiteSpace(txtAdresseServeur.Text))
                errorProvider.SetError(txtAdresseServeur,
                    "L'addresse IP du serveur ne doit pas être vide ou constitué d’espace blanc.");


            //Validation du port
            int port = 0;
            if (!int.TryParse(txtPort.Text, out port) || port < 0)
                errorProvider.SetError(txtPort, "Le port de connexion doit être une valeur numérique entière.");


            foreach (Control ctrl in errorProvider.ContainerControl.Controls)
            {
                string erreur = errorProvider.GetError(ctrl);

                if (!string.IsNullOrWhiteSpace(erreur))
                    messages += $"- {erreur}\n";
            }


            if (messages != "")
            {
                MessageBox.Show($"Veuillez corriger les erreurs suivantes : \n{messages}", "Authentification",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        #endregion
    }
}