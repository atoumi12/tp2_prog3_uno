﻿
namespace _420_14C_FX
{
    partial class frmUtilisateur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gboUtilisateurs = new System.Windows.Forms.GroupBox();
            this.lstUtilisateur = new System.Windows.Forms.ListBox();
            this.gboUtilisateur = new System.Windows.Forms.GroupBox();
            this.txtNomUtilisateur = new System.Windows.Forms.TextBox();
            this.btnNouveau = new System.Windows.Forms.Button();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.btnAjouter = new System.Windows.Forms.Button();
            this.txtMotPasse = new System.Windows.Forms.TextBox();
            this.lblMotDePasse = new System.Windows.Forms.Label();
            this.lblAdresseIP = new System.Windows.Forms.Label();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lblStatut = new System.Windows.Forms.ToolStripStatusLabel();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblStatusBar = new System.Windows.Forms.ToolStripStatusLabel();
            this.gboUtilisateurs.SuspendLayout();
            this.gboUtilisateur.SuspendLayout();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // gboUtilisateurs
            // 
            this.gboUtilisateurs.Controls.Add(this.lstUtilisateur);
            this.gboUtilisateurs.Location = new System.Drawing.Point(422, 15);
            this.gboUtilisateurs.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gboUtilisateurs.Name = "gboUtilisateurs";
            this.gboUtilisateurs.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gboUtilisateurs.Size = new System.Drawing.Size(266, 306);
            this.gboUtilisateurs.TabIndex = 0;
            this.gboUtilisateurs.TabStop = false;
            this.gboUtilisateurs.Text = "Liste des utilisateurs";
            // 
            // lstUtilisateur
            // 
            this.lstUtilisateur.FormattingEnabled = true;
            this.lstUtilisateur.ItemHeight = 20;
            this.lstUtilisateur.Location = new System.Drawing.Point(19, 26);
            this.lstUtilisateur.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.lstUtilisateur.Name = "lstUtilisateur";
            this.lstUtilisateur.Size = new System.Drawing.Size(227, 264);
            this.lstUtilisateur.TabIndex = 5;
            this.lstUtilisateur.SelectedIndexChanged += new System.EventHandler(this.lstUtilisateur_SelectedIndexChanged);
            // 
            // gboUtilisateur
            // 
            this.gboUtilisateur.Controls.Add(this.txtNomUtilisateur);
            this.gboUtilisateur.Controls.Add(this.btnNouveau);
            this.gboUtilisateur.Controls.Add(this.btnSupprimer);
            this.gboUtilisateur.Controls.Add(this.btnAjouter);
            this.gboUtilisateur.Controls.Add(this.txtMotPasse);
            this.gboUtilisateur.Controls.Add(this.lblMotDePasse);
            this.gboUtilisateur.Controls.Add(this.lblAdresseIP);
            this.gboUtilisateur.Location = new System.Drawing.Point(29, 15);
            this.gboUtilisateur.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gboUtilisateur.Name = "gboUtilisateur";
            this.gboUtilisateur.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gboUtilisateur.Size = new System.Drawing.Size(386, 306);
            this.gboUtilisateur.TabIndex = 1;
            this.gboUtilisateur.TabStop = false;
            this.gboUtilisateur.Text = "Utilisateur";
            // 
            // txtNomUtilisateur
            // 
            this.txtNomUtilisateur.Location = new System.Drawing.Point(144, 49);
            this.txtNomUtilisateur.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtNomUtilisateur.Name = "txtNomUtilisateur";
            this.txtNomUtilisateur.Size = new System.Drawing.Size(199, 26);
            this.txtNomUtilisateur.TabIndex = 0;
            // 
            // btnNouveau
            // 
            this.btnNouveau.Location = new System.Drawing.Point(65, 248);
            this.btnNouveau.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNouveau.Name = "btnNouveau";
            this.btnNouveau.Size = new System.Drawing.Size(100, 44);
            this.btnNouveau.TabIndex = 2;
            this.btnNouveau.Text = "Nouveau";
            this.btnNouveau.UseVisualStyleBackColor = true;
            this.btnNouveau.Click += new System.EventHandler(this.btnNouveau_Click);
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.Location = new System.Drawing.Point(279, 248);
            this.btnSupprimer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(100, 44);
            this.btnSupprimer.TabIndex = 4;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = true;
            this.btnSupprimer.Click += new System.EventHandler(this.btnSupprimer_Click);
            // 
            // btnAjouter
            // 
            this.btnAjouter.Location = new System.Drawing.Point(172, 248);
            this.btnAjouter.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAjouter.Name = "btnAjouter";
            this.btnAjouter.Size = new System.Drawing.Size(100, 44);
            this.btnAjouter.TabIndex = 3;
            this.btnAjouter.Text = "Ajouter";
            this.btnAjouter.UseVisualStyleBackColor = true;
            this.btnAjouter.Click += new System.EventHandler(this.btnAjouter_Click);
            // 
            // txtMotPasse
            // 
            this.txtMotPasse.Location = new System.Drawing.Point(144, 98);
            this.txtMotPasse.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtMotPasse.Name = "txtMotPasse";
            this.txtMotPasse.Size = new System.Drawing.Size(199, 26);
            this.txtMotPasse.TabIndex = 1;
            // 
            // lblMotDePasse
            // 
            this.lblMotDePasse.AutoSize = true;
            this.lblMotDePasse.Location = new System.Drawing.Point(33, 101);
            this.lblMotDePasse.Name = "lblMotDePasse";
            this.lblMotDePasse.Size = new System.Drawing.Size(105, 20);
            this.lblMotDePasse.TabIndex = 4;
            this.lblMotDePasse.Text = "Mot de passe";
            // 
            // lblAdresseIP
            // 
            this.lblAdresseIP.AutoSize = true;
            this.lblAdresseIP.Location = new System.Drawing.Point(19, 55);
            this.lblAdresseIP.Name = "lblAdresseIP";
            this.lblAdresseIP.Size = new System.Drawing.Size(122, 20);
            this.lblAdresseIP.TabIndex = 0;
            this.lblAdresseIP.Text = "Non d\'utilisateur";
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.Location = new System.Drawing.Point(573, 329);
            this.btnAnnuler.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(115, 44);
            this.btnAnnuler.TabIndex = 7;
            this.btnAnnuler.Text = "Annuler";
            this.btnAnnuler.UseVisualStyleBackColor = true;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.Location = new System.Drawing.Point(441, 329);
            this.btnEnregistrer.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(115, 44);
            this.btnEnregistrer.TabIndex = 6;
            this.btnEnregistrer.Text = "Enregistrer";
            this.btnEnregistrer.UseVisualStyleBackColor = true;
            this.btnEnregistrer.Click += new System.EventHandler(this.btnEnregistrer_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatut,
            this.lblStatusBar});
            this.statusStrip.Location = new System.Drawing.Point(0, 391);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            this.statusStrip.Size = new System.Drawing.Size(701, 28);
            this.statusStrip.TabIndex = 8;
            this.statusStrip.Text = "statusStrip1";
            // 
            // lblStatut
            // 
            this.lblStatut.Name = "lblStatut";
            this.lblStatut.Size = new System.Drawing.Size(0, 21);
            // 
            // errorProvider
            // 
            this.errorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProvider.ContainerControl = this;
            // 
            // lblStatusBar
            // 
            this.lblStatusBar.Name = "lblStatusBar";
            this.lblStatusBar.Size = new System.Drawing.Size(0, 21);
            // 
            // frmUtilisateur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 419);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.btnEnregistrer);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.gboUtilisateur);
            this.Controls.Add(this.gboUtilisateurs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmUtilisateur";
            this.Text = "Gestion des utilisateurs";
            this.Load += new System.EventHandler(this.frmUtilisateur_Load);
            this.gboUtilisateurs.ResumeLayout(false);
            this.gboUtilisateur.ResumeLayout(false);
            this.gboUtilisateur.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gboUtilisateurs;
        private System.Windows.Forms.ListBox lstUtilisateur;
        private System.Windows.Forms.GroupBox gboUtilisateur;
        private System.Windows.Forms.Button btnNouveau;
        private System.Windows.Forms.Button btnSupprimer;
        private System.Windows.Forms.Button btnAjouter;
        private System.Windows.Forms.TextBox txtMotPasse;
        private System.Windows.Forms.Label lblMotDePasse;
        private System.Windows.Forms.Label lblAdresseIP;
        private System.Windows.Forms.Button btnAnnuler;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.TextBox txtNomUtilisateur;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lblStatut;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.ToolStripStatusLabel lblStatusBar;
    }
}