﻿
namespace _420_14C_FX
{
    partial class frmServeur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstClients = new System.Windows.Forms.ListBox();
            this.btnDemarrerServeur = new System.Windows.Forms.Button();
            this.btnDemarrerPartie = new System.Windows.Forms.Button();
            this.txtStatut = new System.Windows.Forms.RichTextBox();
            this.Serveur = new System.Windows.Forms.GroupBox();
            this.txtNoPort = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblAdresseIP = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnFermer = new System.Windows.Forms.Button();
            this.btnQuitter = new System.Windows.Forms.Button();
            this.Serveur.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstClients
            // 
            this.lstClients.FormattingEnabled = true;
            this.lstClients.ItemHeight = 16;
            this.lstClients.Location = new System.Drawing.Point(6, 27);
            this.lstClients.Name = "lstClients";
            this.lstClients.Size = new System.Drawing.Size(188, 308);
            this.lstClients.TabIndex = 0;
            // 
            // btnDemarrerServeur
            // 
            this.btnDemarrerServeur.Location = new System.Drawing.Point(238, 57);
            this.btnDemarrerServeur.Name = "btnDemarrerServeur";
            this.btnDemarrerServeur.Size = new System.Drawing.Size(85, 27);
            this.btnDemarrerServeur.TabIndex = 2;
            this.btnDemarrerServeur.Text = "Démarrer";
            this.btnDemarrerServeur.UseVisualStyleBackColor = true;
            this.btnDemarrerServeur.Click += new System.EventHandler(this.btnDemarrerServeur_Click);
            // 
            // btnDemarrerPartie
            // 
            this.btnDemarrerPartie.Enabled = false;
            this.btnDemarrerPartie.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDemarrerPartie.Location = new System.Drawing.Point(6, 346);
            this.btnDemarrerPartie.Name = "btnDemarrerPartie";
            this.btnDemarrerPartie.Size = new System.Drawing.Size(188, 23);
            this.btnDemarrerPartie.TabIndex = 3;
            this.btnDemarrerPartie.Text = "Démarrer la partie";
            this.btnDemarrerPartie.UseVisualStyleBackColor = true;
            this.btnDemarrerPartie.Click += new System.EventHandler(this.btnDemarrerPartie_Click);
            // 
            // txtStatut
            // 
            this.txtStatut.Location = new System.Drawing.Point(19, 90);
            this.txtStatut.Name = "txtStatut";
            this.txtStatut.Size = new System.Drawing.Size(304, 279);
            this.txtStatut.TabIndex = 4;
            this.txtStatut.Text = "";
            // 
            // Serveur
            // 
            this.Serveur.Controls.Add(this.txtNoPort);
            this.Serveur.Controls.Add(this.label1);
            this.Serveur.Controls.Add(this.lblAdresseIP);
            this.Serveur.Controls.Add(this.txtStatut);
            this.Serveur.Controls.Add(this.btnDemarrerServeur);
            this.Serveur.Location = new System.Drawing.Point(27, 33);
            this.Serveur.Name = "Serveur";
            this.Serveur.Size = new System.Drawing.Size(337, 381);
            this.Serveur.TabIndex = 5;
            this.Serveur.TabStop = false;
            this.Serveur.Text = "Serveur";
            // 
            // txtNoPort
            // 
            this.txtNoPort.Location = new System.Drawing.Point(56, 62);
            this.txtNoPort.Name = "txtNoPort";
            this.txtNoPort.Size = new System.Drawing.Size(100, 22);
            this.txtNoPort.TabIndex = 7;
            this.txtNoPort.Text = "100";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Port";
            // 
            // lblAdresseIP
            // 
            this.lblAdresseIP.AutoSize = true;
            this.lblAdresseIP.Location = new System.Drawing.Point(16, 34);
            this.lblAdresseIP.Name = "lblAdresseIP";
            this.lblAdresseIP.Size = new System.Drawing.Size(88, 17);
            this.lblAdresseIP.TabIndex = 5;
            this.lblAdresseIP.Text = "Adresse IP : ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lstClients);
            this.groupBox1.Controls.Add(this.btnDemarrerPartie);
            this.groupBox1.Location = new System.Drawing.Point(379, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 381);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Client(s) connecté(s)";
            // 
            // btnFermer
            // 
            this.btnFermer.Location = new System.Drawing.Point(400, 420);
            this.btnFermer.Name = "btnFermer";
            this.btnFermer.Size = new System.Drawing.Size(85, 27);
            this.btnFermer.TabIndex = 7;
            this.btnFermer.Text = "Cacher";
            this.btnFermer.UseVisualStyleBackColor = true;
            this.btnFermer.Click += new System.EventHandler(this.btnFermer_Click);
            // 
            // btnQuitter
            // 
            this.btnQuitter.Location = new System.Drawing.Point(494, 420);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(85, 27);
            this.btnQuitter.TabIndex = 8;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.btnQuitter_Click);
            // 
            // frmServeur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(590, 458);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.btnFermer);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Serveur);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmServeur";
            this.Text = "frmServeur";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmServeur_FormClosing);
            this.Load += new System.EventHandler(this.frmServeur_Load);
            this.Serveur.ResumeLayout(false);
            this.Serveur.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstClients;
        private System.Windows.Forms.Button btnDemarrerServeur;
        private System.Windows.Forms.Button btnDemarrerPartie;
        private System.Windows.Forms.RichTextBox txtStatut;
        private System.Windows.Forms.GroupBox Serveur;
        private System.Windows.Forms.TextBox txtNoPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblAdresseIP;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnFermer;
        private System.Windows.Forms.Button btnQuitter;
    }
}