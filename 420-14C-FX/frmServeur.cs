﻿#region MÉTADONNÉES

// Nom du fichier : frmServeur.cs
// Auteur : Martin Vézina (MartinVézina)
// Date de création : 2021-02-19
// Date de modification : 2021-03-10

#endregion

#region USING

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using _420_14C_FX.Classes;

#endregion

namespace _420_14C_FX
{
    public partial class frmServeur : Form
    {
        #region CONSTANTES ET ATTRIBUTS STATIQUES

        /// <summary>
        /// Taille du buffer pour l'envoi de données
        /// </summary>
        public const int TAILLE_BUFFER = 1024 * 1024;

        #endregion

        #region ATTRIBUTS

        /// <summary>
        /// Partie envoyée au clients
        /// </summary>
        private Partie _partie;

        /// <summary>
        /// Buffer contenant les données 
        /// </summary>
        private byte[] _buffer = new byte[frmServeur.TAILLE_BUFFER];


        /// <summary>
        /// Liste des sockets clients
        /// </summary>
        private List<Socket> _socketsClient;


        /// <summary>
        /// Socket serveur
        /// </summary>
        private Socket _socketServeur;

        /// <summary>
        /// Indique si la fenêtre a été fermée
        /// </summary>
        private bool _fermer;

        #endregion

        #region CONSTRUCTEURS

        public frmServeur()
        {
            InitializeComponent();
        }

        #endregion

        #region MÉTHODES

        private void frmServeur_Load(object sender, EventArgs e)
        {
            lblAdresseIP.Text += $"{Utilitaire.ObtenirAdresseIpLocale()}";
        }


        /// <summary>
        /// Permet la configuration du serveur.
        /// </summary>
        private void ConfigurerServer()
        {
            //Initialisation du socker serveur.
            _socketServeur = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            //connexion du socket serveur au port spécifié pour l'acceptation des connexions.
            _socketServeur.Bind(new IPEndPoint(IPAddress.Any, int.Parse(txtNoPort.Text)));
            _socketServeur.Listen(1);

            //On commence à écouter sur le port
            _socketServeur.BeginAccept(new AsyncCallback(AcceptConnexionCallback), null);
            MiseAJourStatut("En attente de connexions . . .");

            //Initialisation de la liste des sockets clients.
            _socketsClient = new List<Socket>();


            //Initialisation d'une nouvelle partie.
            _partie = new Partie();
        }

        /// <summary>
        /// Permet l'acceptation des demmandes de connexion
        /// </summary>
        /// <param name="ar"></param>
        private void AcceptConnexionCallback(IAsyncResult ar)
        {
            if (_socketServeur != null)
            {
                //Arrêt de la réception des demandes de connexion
                Socket socket = _socketServeur.EndAccept(ar);

                //Ajout du nouveau socket client
                _socketsClient.Add(socket);

                //Affichage de la liste des clients connectés
                AfficherListeClients();


                //Mise à jour du statut du serveur
                MiseAJourStatut($"Nb. clients connecté(s) : {_socketsClient.Count}");


                //On permet le démarrage de la partie s'il y au moins une connexion.
                btnDemarrerPartie.Invoke(new Action(() => { btnDemarrerPartie.Enabled = true; }));


                //On écoute pour la réception de données du client
                socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback),
                    socket);

                //On écoute pour d'autre connexions
                _socketServeur.BeginAccept(new AsyncCallback(AcceptConnexionCallback), null);
            }

        }


        /// <summary>
        /// Permet de recevoir les données envoyées par les clients
        /// </summary>
        /// <param name="ar"></param>
        private void ReceiveCallback(IAsyncResult ar)
        {
            //Socket client qui a envoyé les données
            Socket socket = (Socket)ar.AsyncState;

            if (socket.Connected)
            {
                int received;
                try
                {
                    received = socket.EndReceive(ar);


                    //Si des données ont été reçues
                    if (received != 0)
                    {
                        byte[] dataBuf = new byte[received];
                        Array.Copy(_buffer, dataBuf, received);

                        //Désérialisation des données reçues.
                        Object obj = Utilitaire.Deserialiser(dataBuf);

                        if (obj is JetonAuthentification jeton)
                        {

                            MiseAJourStatut(
                                $"Authentification de {jeton.NomUtilisateur}.....");

                            Authentifier(jeton);

                            if (jeton.Authenfie)
                                MiseAJourStatut($"Le joueur {jeton.NomUtilisateur} a été authentifié avec succès!");


                            else
                            {
                                MiseAJourStatut($"Erreur d'authetification du joueur {jeton.NomUtilisateur}\n");
                                _socketsClient.Remove(socket);
                            }

                            EnvoyerDonnees(socket, jeton);
                        }

                        if (obj is Partie partie)
                        {
                            foreach (Socket client in _socketsClient)
                            {
                                EnvoyerDonnees(client, partie);
                            }
                        }

                        //Mise à jour de la liste des clients connectés
                        AfficherListeClients();
                    }


                }
                catch (Exception)
                {
                    //Si le client est déconnecté on le retire de la liste
                    for (int i = 0; i < _socketsClient.Count; i++)
                    {
                        if (_socketsClient[i].RemoteEndPoint.ToString().Equals(socket.RemoteEndPoint.ToString()))
                        {
                            _socketsClient.RemoveAt(i);
                            MiseAJourStatut($"Client déconnecté : {socket.RemoteEndPoint}");
                        }
                    }

                    return;
                }


            }

            //On recommence à accepter des données.
            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback),
                socket);
        }


        private void MiseAJourStatut(string pMessage)
        {
            //Mise à jour du statut du serveur
            txtStatut.Invoke(new Action(() => { txtStatut.AppendText($" {pMessage} \r\n"); }));
        }

        /// <summary>
        /// Permet l'affichage de la liste des clients connectés.
        /// </summary>
        private void AfficherListeClients()
        {
            //Ajout de l'addresse ip du client à la liste
            lstClients.Invoke(new Action(() =>
            {
                lstClients.Items.Clear();

                for (int i = 0; i < _socketsClient.Count; i++)
                {
                    lstClients.Items.Add(_socketsClient[i].RemoteEndPoint.ToString());
                }
            }));
        }

        /// <summary>
        /// Permet d'authentifier un joueur.
        /// </summary>
        /// <param name="pJeton">Le joueur qui se connecte</param>
        /// <remarks>Le jeton est mise à jour selon le résultat de l'authentification. Si l'authentification réussie, alors un nouveau joueur est créé et ajouté à la partie.</remarks>
        public void Authentifier(JetonAuthentification pJeton)
        {
            if (_partie == null)
                _partie = new Partie();

            Dictionary<string, (byte[], byte[])> dUtilisateurs = Utilitaire.ObtenirUtilisateurs();

            if (dUtilisateurs.ContainsKey(pJeton.NomUtilisateur))
            {
                if (Utilitaire.VerifierMdp(pJeton.MotPasse, dUtilisateurs[pJeton.NomUtilisateur].Item1,
                    dUtilisateurs[pJeton.NomUtilisateur].Item2))
                {
                    pJeton.Authenfie = true;
                    _partie.Joueurs.AjouterDebut(new Joueur(pJeton.NomUtilisateur));
                }
            }


        }

        /// <summary>
        /// Permet l'envoi d'un objet sérialisé à un client 
        /// </summary>
        /// <param name="socket">Socket client à qui sera envoyé l'objet</param>
        /// <param name="obj">Objet à sérialisé et à envoyer</param>
        void EnvoyerDonnees(Socket socket, Object obj)
        {
            //Sérialisation de l'objet à envoyer
            byte[] data = Utilitaire.Serialiser(obj);

            //Initialisation de l'envoie des données.
            socket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(SendCallback), socket);


            _socketServeur.BeginAccept(new AsyncCallback(AcceptConnexionCallback), null);
        }



        private void SendCallback(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            socket.EndSend(AR);
        }

        /// <summary>
        /// Permet le démarrage du serveur.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDemarrerServeur_Click(object sender, EventArgs e)
        {
            MiseAJourStatut("Démarrage du serveur . . .");

            ConfigurerServer();

            //On désactive la possibiliter de redémarrer le serveur
            btnDemarrerServeur.Enabled = false;
        }

        /// <summary>
        /// Permet de démarrer une partie.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>La partie est démarrée et envoyée à tous les clients.</remarks>
        private void btnDemarrerPartie_Click(object sender, EventArgs e)
        {

            if (_partie.DemarrerPartie())
            {
                foreach (Socket clientSocket in _socketsClient)
                {
                    EnvoyerDonnees(clientSocket, _partie);
                }
            }
            else
            {
                MessageBox.Show("Impossible de démarrer la partie. Il n'y a aucun joueur d'authentifié!");
            }

        }

        private void btnFermer_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmServeur_FormClosing(object sender, FormClosingEventArgs e)
        {
            //On s'assure que l'utilisateur a bien voulu quitter et fermer le serveur. 
            //sinon, on chache la fenêtre.
            if (_fermer)
            {
                try
                {
                    if (_socketServeur.Connected)
                        _socketServeur.Shutdown(SocketShutdown.Both);

                }
                finally
                {
                    _socketServeur.Close();
                }

                _socketServeur.Dispose();
                _socketServeur = null;

            }

            else
            {
                e.Cancel = true;
                Hide();
            }


        }

        #endregion

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            //On vérifie si le seveur est démarré
            if (_socketServeur != null)
            {
                //Si oui, on indique à l'utilsateur que cela va arrêter le seveur.
                DialogResult resultat =
                    MessageBox.Show("Cette action arrêtera le serveur. êtes-vous certain de vouloir quitter?",
                        "Arrêt du serveur", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                _fermer = (resultat == DialogResult.Yes);

            }

            Close();

        }
    }
}