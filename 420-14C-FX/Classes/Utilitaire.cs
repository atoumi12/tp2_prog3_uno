﻿#region MÉTADONNÉES

// Nom du fichier : Utilitaire.cs
// Auteur : Ahmed Toumi (ahmed)
// Date de création : 2021-04-02
// Date de modification : 2021-04-09

#endregion

#region USING

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using Konscious.Security.Cryptography;

#endregion

namespace _420_14C_FX.Classes
{
    /// <summary>
    /// Classe utilitaire permettant de sérialiser et desérialiser des objets
    /// </summary>
    public static class Utilitaire
    {
        #region CONSTANTES ET ATTRIBUTS STATIQUES

        private const string FICHIER_UTIL = "utilisateurs.dat";

        #endregion

        #region MÉTHODES

        /// <summary>
        /// Permet d'obtenir la liste des utilisateurs.
        /// </summary>
        /// <returns>Dictionnaire de (nom, (salt, motPasse))></returns>
        public static Dictionary<string, (byte[], byte[])> ObtenirUtilisateurs()
        {
            Dictionary<string, (byte[], byte[])> dUtilisateurs = new Dictionary<string, (byte[], byte[])>();

            if (File.Exists(Utilitaire.FICHIER_UTIL))
            {
                using (StreamReader sr = new StreamReader(Utilitaire.FICHIER_UTIL))
                {
                    string[] vectLignes = sr.ReadToEnd().Split('\n');

                    for (int i = 0; i < vectLignes.Length; i++)
                    {
                        if (vectLignes[i] != "")
                        {
                            string[] vectChamps = vectLignes[i].Split(';');
                            dUtilisateurs.Add(vectChamps[0], (Convert.FromBase64String(vectChamps[1]), Convert.FromBase64String(vectChamps[2])));
                        }

                    }

                    sr.Close();
                }

            }

            return dUtilisateurs;
        }

        /// <summary>
        /// Permet de sauvegarder les utilisateurs dans le fichier .dat
        /// </summary>
        /// <param name="pUtilisateurs">Dictionnaire de (nom, (salt, motPasse)) contenant les informations des utlisateurs.</param>
        public static void SauvegarderUtilisateurs(Dictionary<string, (byte[], byte[])> pUtilisateurs)
        {
            using (StreamWriter sw = new StreamWriter(Utilitaire.FICHIER_UTIL))
            {
                foreach (KeyValuePair<string, (byte[], byte[])> item in pUtilisateurs)
                {
                    sw.Write($"{item.Key};{Convert.ToBase64String(item.Value.Item1)};{Convert.ToBase64String(item.Value.Item2)}\n");
                }
            }


        }

        /// <summary>
        /// Permet d'obtenir l'adresse Ip de l'ordinateur.
        /// </summary>
        /// <returns>Adresse Ip de l'ordinateur.</returns>
        public static string ObtenirAdresseIpLocale()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }

            return null;
        }

        #endregion

        #region Serialisation et désérialisation

        /// <summary>
        /// Permet de sérialiser un objet
        /// </summary>
        /// <param name="pObjetSerialisable">Objet à sérialiser</param>
        /// <returns>Vecteur de bits représentant l'objet sérialisé</returns>
        public static Byte[] Serialiser(object pObjetSerialisable)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                (new BinaryFormatter()).Serialize(memoryStream, pObjetSerialisable);
                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Permet de désérialiser un objet.
        /// </summary>
        /// <param name="pBytes">Vecteur de bits</param>
        /// <returns>Un objet</returns>
        public static object Deserialiser(Byte[] pBytes)
        {
            using (MemoryStream memoryStream = new MemoryStream(pBytes))
                return (new BinaryFormatter()).Deserialize(memoryStream);
        }

        #endregion

        #region Salt et Hash

        /// <summary>
        /// Permet de géner un salt
        /// </summary>
        /// <returns>Un vecteur de byte représentant le salt.</returns>
        public static byte[] GenererSalt()
        {
            byte[] buffer = new byte[16];
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(buffer);
            return buffer;
        }

        /// <summary>
        /// Permet de hacher un mot de passe
        /// </summary>
        /// <param name="pMotPasse">Mot de passe</param>
        /// <param name="pSalt">Salt</param>
        /// <returns>Vecteur de byte présentant le hachage du mot de passe.</returns>
        public static byte[] HashMotDePasse(string pMotPasse, byte[] pSalt)
        {
            Argon2id argon2 = new Argon2id(Encoding.UTF8.GetBytes(pMotPasse))
            {
                Salt = pSalt,
                DegreeOfParallelism = 8,
                Iterations = 4,
                MemorySize = 1024 * 1024
            };

            GC.Collect();

            return argon2.GetBytes(16);
        }

        /// <summary>
        /// Permet de vérifier la validiter d'un mot de passe.
        /// </summary>
        /// <param name="pMotPasse">Mot de passe de l'utilisateur</param>
        /// <param name="pSalt">Salt de l'utilisateur</param>
        /// <param name="pHash">Hash du mot de passe de l'utilisateur</param>
        /// <returns>True si le nouveau hash du mot de passe correspond à celui reçu en paramètre.</returns>
        public static bool VerifierMdp(string pMotPasse, byte[] pSalt, byte[] pHash)
        {
            byte[] nouveauHash = Utilitaire.HashMotDePasse(pMotPasse, pSalt);
            return pHash.SequenceEqual(nouveauHash);
        }

        #endregion
    }
}