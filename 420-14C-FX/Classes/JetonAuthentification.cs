﻿#region MÉTADONNÉES

// Nom du fichier : JetonAuthentification.cs
// Auteur : Ahmed Toumi (ahmed)
// Date de création : 2021-04-06
// Date de modification : 2021-04-06

#endregion

#region USING

using System;

#endregion

namespace _420_14C_FX.Classes
{
    /// <summary>
    /// Représente un jeton d’authentification pour authentifier un utilisateur sur le serveur.
    /// </summary>
     [SerializableAttribute]
    public class JetonAuthentification
    {
        #region CONSTANTES ET ATTRIBUTS STATIQUES

        /// <summary>
        /// Nombre miminale de carateres dans le nom d'utilisateur.
        /// </summary>
        public const int NB_MIN_CARACTERES_NOM = 3;

        /// <summary>
        ///  Nombre miminale de carateres dans le mot de passe de l'utilisateur.
        /// </summary>
        public const int NB_MIN_CARACTERES_MDP = 5;

        #endregion

        #region ATTRIBUTS

        private string _nomUtilisateur;
        private string _motPasse;
        private bool _authenfie;

        #endregion

        #region PROPRIÉTÉS ET INDEXEURS

        /// <summary>
        /// Obtient ou définit le nom d'utilisateur.
        /// </summary>
        ///<exception cref="ArgumentNullException">Lancée lorsque le nom d'utilisateur est vide ou nul</exception>
        /// <exception cref="ArgumentOutOfRangeException">Lancée lorsque le nombre de caractère est plus petit que NB_MIN_CARACTERES_NOM </exception>
        public string NomUtilisateur
        {
            get { return _nomUtilisateur; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("Nom", "Le nom du joueur ne peux pas être null ou vide");

                if (value.Trim().Length < JetonAuthentification.NB_MIN_CARACTERES_NOM)
                    throw new ArgumentOutOfRangeException("Nom",
                        $"Le nom d'utilisateur doit contenir minimum {JetonAuthentification.NB_MIN_CARACTERES_NOM} caractères ");

                _nomUtilisateur = value;
            }
        }


        /// <summary>
        /// Obtient ou définit le mot de passe de l'utilisateur.
        /// </summary>
        ///<exception cref="ArgumentNullException">Lancée lorsque le mot de passe de l'utilisateur est vide ou nul</exception>
        /// <exception cref="ArgumentOutOfRangeException">Lancée lorsque le nombre de caractère est plus petit que NB_MIN_CARACTERES_MDP </exception>
        public string MotPasse
        {
            get { return _motPasse; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("Mot de passe",
                        "Le mot de passe de l'utilisateur ne peux pas être null ou vide");

                if (value.Trim().Length < JetonAuthentification.NB_MIN_CARACTERES_MDP)
                    throw new ArgumentOutOfRangeException("Mot de passe",
                        $"Le mot de passe de l'utilisateur doit contenir au moins {JetonAuthentification.NB_MIN_CARACTERES_MDP} caractères");


                _motPasse = value;
            }
        }

        public bool Authenfie
        {
            get { return _authenfie; }
            set { _authenfie = value; }
        }

        #endregion

        #region CONSTRUCTEURS

        /// <summary>
        /// Permet de créer un jeton avec un nom d’utilisateur et un mot de passe.
        /// </summary>
        /// <param name="pNomUtilisateur">Le nom d'utilisateur</param>
        /// <param name="pMotPasse">le mot de passe de l'utilisateur</param>
        public JetonAuthentification(string pNomUtilisateur, string pMotPasse)
        {
            NomUtilisateur = pNomUtilisateur;
            MotPasse = pMotPasse;
            Authenfie = false;
        }

        #endregion
    }
}