﻿#region MÉTADONNÉES

// Nom du fichier : Carte.cs
// Auteur : Ahmed Toumi (ahmed)
// Date de création : 2021-04-02
// Date de modification : 2021-04-02

#endregion

#region USING

using System;

#endregion

namespace _420_14C_FX.Classes
{
    /// <summary>
    /// Classe représentant une carte du jeu Uno.
    /// </summary>
    [SerializableAttribute]
    public class Carte : IComparable
    {
        #region ATTRIBUTS

        private Couleur _couleur;
        private Valeur _valeur;
        private byte _points;

        #endregion

        #region PROPRIÉTÉS ET INDEXEURS

        /// <summary>
        /// Obtient ou définit la courleur de la carte
        /// </summary>
        public Couleur Couleur
        {
            get { return _couleur; }
            set { _couleur = value; }
        }

        /// <summary>
        /// Obtient ou définit la valeur de la carte
        /// </summary>
        public Valeur Valeur
        {
            get { return _valeur; }
            set { _valeur = value; }
        }

        /// <summary>
        /// Obtient ou définit les points de la carte 
        /// </summary>
        public byte Points
        {
            get { return _points; }
            set { _points = value; }
        }

        #endregion

        #region CONSTRUCTEURS

        /// <summary>
        /// Constructeur d'une carte
        /// </summary>
        /// <param name="pCouleur">Courleur de la carte</param>
        /// <param name="pValeur">Valeur de la carte</param>
        /// <param name="pPoints">Points de la carte</param>
        public Carte(Couleur pCouleur, Valeur pValeur, byte pPoints)
        {
            Couleur = pCouleur;
            Valeur = pValeur;
            Points = pPoints;

        }

        #endregion


        /// <summary>
        /// Permet de comparer deux cartes entre elles
        /// </summary>
        /// <param name="obj">La carte avec laquelle l'instance courante est comparée</param>
        /// <returns>-1 si la carte est plus petite, 0 si égale, 1 si plus grande</returns>
        public int CompareTo(object obj)
        {
            int comparaisonCouleur = Couleur.CompareTo(((Carte)obj).Couleur);
            int comparaisonValeur = Valeur.CompareTo(((Carte)obj).Valeur);

            int resultat = int.MaxValue;

            if (comparaisonCouleur == -1)
                resultat = -1;
            else if (comparaisonCouleur == 1)
                resultat = 1;
            else if (comparaisonCouleur == 0)
            {
                if (comparaisonValeur == -1)
                    resultat = -1;
                else if (comparaisonValeur == 1)
                    resultat = 1;
                else
                    resultat = 0;
            }


            return resultat;

        }
    }
}