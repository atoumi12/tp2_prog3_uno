﻿#region MÉTADONNÉES

// Nom du fichier : Valeur.cs
// Auteur : Martin Vézina (MartinVézina)
// Date de création : 2021-02-15
// Date de modification : 2021-03-10

#endregion

namespace _420_14C_FX.Classes
{
    /// <summary>
    /// Représente les valeurs des cartes du jeu Uno
    /// </summary>
    public enum Valeur
    {
        Zero,
        Un,
        Deux,
        Trois,
        Quatre,
        Cinq,
        Six,
        Sept,
        Huit,
        Neuf,
        Plus2,
        InverserSens,
        SauterTour,
        Plus4,
        Joker
    }
}