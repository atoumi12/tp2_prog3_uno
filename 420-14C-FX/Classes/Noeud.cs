﻿#region MÉTADONNÉES

// Nom du fichier : Noeud.cs
// Auteur : Ahmed Toumi (ahmed)
// Date de création : 2021-04-02
// Date de modification : 2021-04-02

#endregion

using System;

namespace _420_14C_FX.Classes
{
    /// <summary>
    /// Représente un nœud de la liste de joueurs.
    /// </summary>
    [SerializableAttribute]
    public class Noeud
    {
        #region ATTRIBUTS

        private Joueur _valeur;
        private Noeud _suivant;
        private Noeud _precedent;

        #endregion

        #region PROPRIÉTÉS ET INDEXEURS

        /// <summary>
        /// Représente la valeur du nœud qui est un joueur
        /// </summary>
        public Joueur Valeur
        {
            get { return _valeur; }
            set { _valeur = value; }
        }

        /// <summary>
        /// Représente le nœud suivant dans la liste.
        /// </summary>
        public Noeud Suivant
        {
            get { return _suivant; }
            set { _suivant = value; }
        }


        /// <summary>
        /// Représente le nœud précédent.
        /// </summary>
        public Noeud Precedent
        {
            get { return _precedent; }
            set { _precedent = value; }
        }

        #endregion

        #region CONSTRUCTEURS

        /// <summary>
        /// Permet de construire un nouveau nœud.
        /// </summary>
        /// <param name="pValeur">la valeur est le joueur reçu en paramètre</param>
        public Noeud(Joueur pValeur)
        {
            Valeur = pValeur;
            Suivant = null;
            Precedent = null;
        }

        #endregion
    }
}