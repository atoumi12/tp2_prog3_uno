﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _420_14C_FX.Classes
{
    /// <summary>
    /// Représente une liste circulaire liée de joueurs
    /// </summary>
    [SerializableAttribute]
    public class ListeJoueurs
    {
        private Noeud _debut;
        private int _taile;


        /// <summary>
        /// Permet d’obtenir le noeud du début de la liste
        /// </summary>
        public Noeud Debut
        {
            get { return _debut; }
            set { _debut = value; }
        }

        /// <summary>
        /// Permet d’obtenir la taille de la liste.
        /// </summary>
        public int Taile
        {
            get { return _taile; }
            set { _taile = value; }
        }


        /// <summary>
        /// Constructeur sans paramètre initialisant une liste vide
        /// </summary>
        public ListeJoueurs()
        {
            Effacer();
        }



        /// <summary>
        /// Permet d’ajouter un joueur au début de la liste.
        /// </summary>
        /// <param name="pJoueur"></param>
        public void AjouterDebut(Joueur pJoueur)
        {
            Noeud noeud = new Noeud(pJoueur);

            if (Debut == null)
                Debut = noeud;

            else
            {

                Noeud noeudCourant = Debut;

                if (Debut.Precedent == null)
                {
                    Debut = noeud;

                    Debut.Precedent = noeudCourant;
                    Debut.Suivant = noeudCourant;

                    noeudCourant.Precedent = Debut;
                    noeudCourant.Suivant = Debut;
                }
                else
                {
                    Noeud premierNoeud = Debut;
                    Noeud dernierNoeud = Debut.Precedent;

                    Debut = noeud;
                    Debut.Precedent = dernierNoeud;
                    Debut.Suivant = premierNoeud;

                    premierNoeud.Precedent = Debut;
                    dernierNoeud.Suivant = Debut;


                }


            }

            Taile++;
        }


        /// <summary>
        /// Permet de trouver le joueur n’ayant plus de carte dans sa main.
        /// </summary>
        /// <returns>Le joueur gagnant</returns>
        public Joueur TrouverGagnant()
        {
            if (Debut == null)
                throw new InvalidOperationException("La liste des joueurs est nulle");


            Noeud noeudCourant = Debut;


            for (int i = 0; i < Taile; i++)
            {
                if (noeudCourant.Valeur.Main.Count == 0 && noeudCourant.Valeur.Uno)
                    return noeudCourant.Valeur;

                noeudCourant = noeudCourant.Suivant;

            }

            return null;

        }

        /// <summary>
        /// Permet de réinitialiser la liste vide.
        /// </summary>
        public void Effacer()
        {
            Debut = null;
            Taile = 0;
        }
    }
}
