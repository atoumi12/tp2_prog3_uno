﻿#region MÉTADONNÉES

// Nom du fichier : JeuCarte.cs
// Auteur : Ahmed Toumi (ahmed)
// Date de création : 2021-04-02
// Date de modification : 2021-04-02

#endregion

#region USING

using System;
using System.Collections.Generic;

#endregion

namespace _420_14C_FX.Classes
{
    /// <summary>
    /// Classe représentant un jeu de carte Uno.
    /// </summary>
    [SerializableAttribute]
    public class JeuCarte
    {
        #region ATTRIBUTS

        private Stack<Carte> _cartes;
        private int _nbCartes;

        #endregion

        #region PROPRIÉTÉS ET INDEXEURS

        /// <summary>
        /// Pile de cartes
        /// </summary>
        public Stack<Carte> Cartes
        {
            get { return _cartes; }
            set { _cartes = value; }
        }

        /// <summary>
        /// Obtient le nombre de cartes dans la pile.
        /// </summary>
        public int NbCartes
        {
            get { return _cartes.Count; }
        }

        #endregion

        #region CONSTRUCTEURS

        /// <summary>
        /// Constructeur 
        /// </summary>
        /// <remarks>Ajoute les cartes à la pile et mélange le jeu.</remarks>
        public JeuCarte()
        {
            Cartes = new Stack<Carte>();
            // Une seule carte 0 
            Cartes.Push(new Carte(Couleur.Rouge, Valeur.Zero, 0));
            Cartes.Push(new Carte(Couleur.Vert, Valeur.Zero, 0));
            Cartes.Push(new Carte(Couleur.Jaune, Valeur.Zero, 0));
            Cartes.Push(new Carte(Couleur.Bleu, Valeur.Zero, 0));


            for (int i = 0; i < 2; i++)
            {
                // Cartes normales
                for (byte j = 1; j <= 9; j++)
                {
                    Cartes.Push(new Carte(Couleur.Rouge, (Valeur)j, j));
                    Cartes.Push(new Carte(Couleur.Vert, (Valeur)j, j));
                    Cartes.Push(new Carte(Couleur.Jaune, (Valeur)j, j));
                    Cartes.Push(new Carte(Couleur.Bleu, (Valeur)j, j));
                }

                // +2
                Cartes.Push(new Carte(Couleur.Rouge, Valeur.Plus2, 20));
                Cartes.Push(new Carte(Couleur.Vert, Valeur.Plus2, 20));
                Cartes.Push(new Carte(Couleur.Jaune, Valeur.Plus2, 20));
                Cartes.Push(new Carte(Couleur.Bleu, Valeur.Plus2, 20));

                // Inversion
                Cartes.Push(new Carte(Couleur.Rouge, Valeur.InverserSens, 20));
                Cartes.Push(new Carte(Couleur.Vert, Valeur.InverserSens, 20));
                Cartes.Push(new Carte(Couleur.Jaune, Valeur.InverserSens, 20));
                Cartes.Push(new Carte(Couleur.Bleu, Valeur.InverserSens, 20));

                //Passer Tour
                Cartes.Push(new Carte(Couleur.Rouge, Valeur.SauterTour, 20));
                Cartes.Push(new Carte(Couleur.Vert, Valeur.SauterTour, 20));
                Cartes.Push(new Carte(Couleur.Jaune, Valeur.SauterTour, 20));
                Cartes.Push(new Carte(Couleur.Bleu, Valeur.SauterTour, 20));

                for (int k = 0; k < 2; k++)
                {
                    //joker
                    Cartes.Push(new Carte(Couleur.Noir, Valeur.Joker, 50));
                    //+4
                    Cartes.Push(new Carte(Couleur.Noir, Valeur.Plus4, 50));
                }
            }

            Melanger();
        }

        /// <summary>
        /// Constructeur permettant de créer un jeu de carte à partir d'une pile de cartes existantes (ex. la défausse).
        /// </summary>
        /// <remarks>Remplace les cartes par celles reçue en paramètrer et mélange le jeu</remarks>
        public JeuCarte(Stack<Carte> pCartes)
        {
            Cartes = pCartes;
            Melanger();
        }

        #endregion

        #region MÉTHODES

        /// <summary>
        /// Permet de mélanger le jeu de carte
        /// </summary>
        private void Melanger()
        {
            Stack<Carte> pileCartes = new Stack<Carte>();
            Queue<Carte> fileCartes = new Queue<Carte>();

            while (Cartes.Count > 0)
            {
                for (int i = 1; i <= Cartes.Count; i++)
                {
                    if (Cartes.Count % i == 0)
                        pileCartes.Push(Cartes.Pop());
                    else
                        fileCartes.Enqueue(Cartes.Pop());
                }
            }

            while (fileCartes.Count > 0)
                Cartes.Push(fileCartes.Dequeue());

            while (pileCartes.Count > 0)
                Cartes.Push(pileCartes.Pop());
        }

        /// <summary>
        /// Permet de retirer des cartes dans le jeu de cartes
        /// </summary>
        /// <param name="pNbCartes">Nombre de carte à retirer</param>
        /// <returns>Liste contenant les cartes retirées du jeu de carte</returns>
        public List<Carte> RetirerCartes(int pNbCartes = 1)
        {
            List<Carte> listCartes = new List<Carte>();

            while (Cartes.Count > 0)
            {
                for (int i = 0; i < pNbCartes; i++)
                {
                    listCartes.Add(Cartes.Pop());
                }
            }

            return listCartes;
        }

        #endregion
    }
}