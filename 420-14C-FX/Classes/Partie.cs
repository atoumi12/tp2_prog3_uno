﻿#region MÉTADONNÉES

// Nom du fichier : Partie.cs
// Auteur : Martin Vézina (MartinVézina)
// Date de création : 2021-02-17
// Date de modification : 2021-03-10

#endregion

#region USING

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace _420_14C_FX.Classes
{
    /// <summary>
    /// Permet la gestion d’une partie de Uno.
    /// </summary>
    [SerializableAttribute]
    public class Partie
    {

        private const int NB_CARTES_MAIN = 7;

        #region ATTRIBUTS


        private JeuCarte _pioche;
        private Stack<Carte> _defausse;


        private ListeJoueurs _joueurs;
        private Noeud _joueurCourant;


        /// <summary>
        /// Indique le sens de rotation du jeu.
        /// </summary>
        private bool _sensHoraire = true;

        #endregion

        #region PROPRIÉTÉS ET INDEXEURS

        /// <summary>
        /// Obtient ou définit la liste des joureurs.
        /// </summary>
        public ListeJoueurs Joueurs
        {
            get { return _joueurs; }
            set { _joueurs = value; }
        }


        /// <summary>
        /// Obtient ou définit le jeu la pioche
        /// </summary>
        public JeuCarte Pioche
        {
            get { return _pioche; }
            set { _pioche = value; }
        }

        /// <summary>
        /// Obtient ou définit la pile contenant les cartes jouées
        /// </summary>
        public Stack<Carte> Defausse
        {
            get { return _defausse; }
            set { _defausse = value; }
        }

        /// <summary>
        /// Obtient ou définit le joueur courant
        /// </summary>
        public Noeud JoueurCourant
        {
            get { return _joueurCourant; }
            set { _joueurCourant = value; }
        }



        #endregion

        #region CONSTRUCTEURS

        /// <summary>
        /// Constructeur d'une partie.
        /// </summary>
        public Partie()
        {
            Joueurs = new ListeJoueurs();
        }

        #endregion

        #region MÉTHODES


        /// <summary>
        /// Permet de démarrer une partie. 
        /// </summary>
        /// <returns>True s'il y a au moins un joueru. False sinon.</returns>
        /// <remarks>7 cartes sont distribués (une a la fois) à chaque joueur. Une carte permière carte est tournée sur la défausse et le joueur courant est initialisé. La permière carte tournée ne doit pas être une carte spéciale (+2, +4, Joker, InverserSens ou SauterTour) </remarks>
        public bool DemarrerPartie()
        {
            if (Joueurs.Taile > 1)
            {
                JoueurCourant = Joueurs.Debut;

                Noeud joueur = Joueurs.Debut;

                for (int j = 0; j < Joueurs.Taile; j++)
                {
                    // Vider la main des joueurs.
                    if (joueur.Valeur.Main.Count > 0)
                        joueur.Valeur.Main = new List<Carte>();

                    joueur = joueur.Suivant;
                }


                Pioche = new JeuCarte();
                DistribuerCartes();
                InitialiserDefausse();


                return true;
            }
            else
                return false;


        }



        /// <summary>
        /// Permet de distribuer les cartes aux joueurs.
        /// Chaque joeur se voit distribuer une carte à la fois.
        /// </summary>
        private void DistribuerCartes()
        {

            for (int i = 0; i < Partie.NB_CARTES_MAIN; i++)
            {
                Noeud joueur = Joueurs.Debut;

                for (int j = 0; j < Joueurs.Taile; j++)
                {
                    joueur.Valeur.Main.Add(Pioche.Cartes.Pop());
                    joueur = joueur.Suivant;
                }
            }


        }

        /// <summary>
        /// Permet créer une nouvelle défausse et de tourner la permière carte de la pioche sur la défausse.
        /// </summary>
        /// <remarks>La carte tounée ne doit pas être autre qu'un chiffre.</remarks>
        private void InitialiserDefausse()
        {
            // Initialiser la defausse
            Defausse = new Stack<Carte>();

            Stack<Carte> pileTemp = new Stack<Carte>();

            do
            {
                if ((int)Pioche.Cartes.Peek().Valeur > 9)
                {
                    pileTemp.Push(Pioche.Cartes.Pop());
                }
                else
                {
                    //Recuperer la premiere carte de la defausse pour l'ajouter à la pile après.
                    Carte premiereCarteDefausse = Pioche.Cartes.Pop();

                    while (pileTemp.Count > 0)
                        Pioche.Cartes.Push(pileTemp.Pop());

                    Defausse.Push(premiereCarteDefausse);
                }

            } while (Pioche.Cartes.Count > 0 && Defausse.Count != 1);
        }

        /// <summary>
        /// Permet à un joueur de piger des cartes.
        /// </summary>
        /// <param name="pJoueur">Le joueur qui pige la cartge</param>
        /// <param name="pNbCartes">Le nombre de cartes pigées</param>
        /// <remarks>S'il n'y a plus de carte dans la pioche durant la pige, alors on refait la pioche à partir de la défausse et pige les cartes restantes à piger. La main du joueur doit être triée après la pige et on passe au joueur suivant.</remarks>
        public void PigerCartes(Joueur pJoueur, int pNbCartes)
        {
            if (Pioche.Cartes.Count < pNbCartes)
            {
                Stack<Carte> pileTemp = new Stack<Carte>();

                while (Pioche.Cartes.Count > 0)
                    pileTemp.Push(Pioche.Cartes.Pop());


                //recuperer la premiere cartre de la defausse pour la garder
                Carte premiereCarte = Defausse.Pop();

                while (Defausse.Count > 0)
                    pileTemp.Push(Defausse.Pop());

                // Remettre la première carte de la défausse en place.
                Defausse.Push(premiereCarte);


                // piger les cartes de la pioche au début pour arriver à ceux recupérées de la défausse.
                while (pileTemp.Count > 0)
                    Pioche.Cartes.Push(pileTemp.Pop());

            }

            for (int i = 0; i < pNbCartes; i++)
                pJoueur.Main.Add(Pioche.Cartes.Pop());



            //Passer au prochain joueur.
            JoueurSuivant();
        }



        /// <summary>
        /// Permet à un joueur de joueur une carte
        /// </summary>
        /// <param name="pJoueur">Joueur qui joue son tour</param>
        /// <param name="pCarte">Carte joué par le joueur</param>
        /// <exception cref="InvalidOperationException">Lancée lorsque la carte ne peut pas être joué ou que le joueur n'a pas crié Uno avant de jouer sont avant dernière carte.</exception>
        public void JouerCarte(Joueur pJoueur, Carte pCarte)
        {

            if (pJoueur.Uno && ValiderCarte(pCarte))
            {
                //Retirer la carte de sa main
                pJoueur.Main.Remove(pCarte);
                //Deposer sur la defausse
                Defausse.Push(pCarte);


                // Action dependemment la valeur de la carte
                switch (pCarte.Valeur)
                {
                    case Valeur.Plus2:
                        //Passer au joueur suivant apres avoir joué la carte.
                        JoueurSuivant();
                        PigerCartes(JoueurCourant.Valeur, 2);
                        break;
                    case Valeur.Plus4:
                        //Passer au joueur suivant apres avoir joué la carte.
                        JoueurSuivant();
                        PigerCartes(JoueurCourant.Valeur, 4);
                        break;

                    case Valeur.InverserSens:
                        _sensHoraire = !_sensHoraire;
                        JoueurSuivant();
                        break;

                    case Valeur.SauterTour:
                        //Sauter le tour du jour suivant, on passe directement à celui d'après.
                        JoueurSuivant();
                        JoueurSuivant();
                        break;
                    default:
                        JoueurSuivant();
                        break;
                   

                }
              


            }
            else
                throw new InvalidOperationException("Erreur la carte ne peut pas etre jouée");


        }



        /// <summary>
        /// Permet de se déplacer dans la liste selon le sens du jeu pour sélectionner le joueur suivant.
        /// </summary>
        public void JoueurSuivant()
        {
            if (Joueurs.Taile > 1)
            {
                if (_sensHoraire)
                    JoueurCourant = JoueurCourant.Suivant;
                else
                    JoueurCourant = JoueurCourant.Precedent;
            }
            else
                throw new InvalidOperationException("La partie doit contenir au moins 2 joueurs.");
        }


        /// <summary>
        /// Permet de valider si la carte jouer est valide selon la carte qui se trouve sur la défausse.
        /// </summary>
        /// <param name="pCarte">Carte jouée</param>
        /// <returns>True si la carte est valide, False sinon</returns>
        private bool ValiderCarte(Carte pCarte)
        {
            return (pCarte.Couleur == Defausse.Peek().Couleur || pCarte.Valeur == Defausse.Peek().Valeur ||
                    pCarte.Couleur is Couleur.Noir);

        }

        #endregion
    }
}