﻿#region MÉTADONNÉES

// Nom du fichier : Joueur.cs
// Auteur : Ahmed Toumi (ahmed)
// Date de création : 2021-04-02
// Date de modification : 2021-04-02

#endregion

#region USING

using System;
using System.Collections.Generic;

#endregion

namespace _420_14C_FX.Classes
{
    /// <summary>
    /// Classe représentant un joueur de Uno
    /// </summary>
    [SerializableAttribute]
    public class Joueur
    {
        #region CONSTANTES ET ATTRIBUTS STATIQUES

        /// <summary>
        /// Nombre de caractères minimum pour le nom d'un joueur.
        /// </summary>
        public const int NOM_NB_CARAC_MIN = 3;

        #endregion

        #region ATTRIBUTS

        private string _nom;
        private bool _uno;
        private List<Carte> main;

        #endregion

        #region PROPRIÉTÉS ET INDEXEURS

        /// <summary>
        /// Obtient ou définit le nom du joueur.
        /// </summary>
        ///  /// <exception cref="ArgumentNullException">Lancée lorsque le nom du joueur est vide ou nul</exception>
        /// <exception cref="ArgumentOutOfRangeException">Lancée lorsque le nombre de caractères est plus petit que NOM_NB_CARAC_MIN </exception>
        public string Nom
        {
            get { return _nom; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("Nom", "Le nom du joueur ne peut pas être null");
                if (value.Trim().Length < Joueur.NOM_NB_CARAC_MIN)
                    throw new ArgumentOutOfRangeException("Nom",
                        $"Le nom doit contenir au minimum {Joueur.NOM_NB_CARAC_MIN} caractères ");


                _nom = value;
            }
        }

        /// <summary>
        /// Obtient ou définit si un jouer a dit Uno!
        /// </summary>
        public bool Uno
        {
            get { return _uno; }
            set
            {
                _uno = value;
            }
        }

        /// <summary>
        /// Obtient ou définit les cartes du joueur.
        /// </summary>
        public List<Carte> Main
        {
            get { return main; }
            set { main = value; }
        }

        #endregion

        #region CONSTRUCTEURS

        /// <summary>
        /// Constructeur permettant de créer un joueur.
        /// </summary>
        /// <param name="pNom">Nom du joueur</param>
        public Joueur(string pNom)
        {
            Nom = pNom;
            Uno = true;
            Main = new List<Carte>();
        }

        #endregion

        #region MÉTHODES

        public override string ToString()
        {
            return Nom;
        }

        #endregion
    }
}