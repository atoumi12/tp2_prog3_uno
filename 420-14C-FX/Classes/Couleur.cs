﻿#region MÉTADONNÉES

// Nom du fichier : Couleur.cs
// Auteur : Martin Vézina (MartinVézina)
// Date de création : 2021-02-15
// Date de modification : 2021-03-10

#endregion

namespace _420_14C_FX.Classes
{
    /// <summary>
    /// Représente les couleurs des cartes du jeu Uno
    /// </summary>
    public enum Couleur
    {
        Rouge,
        Vert,
        Jaune,
        Bleu,
        Noir
    }
}