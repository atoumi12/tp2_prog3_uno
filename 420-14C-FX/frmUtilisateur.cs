﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Windows.Forms;
using _420_14C_FX.Classes;

namespace _420_14C_FX
{
    public partial class frmUtilisateur : Form
    {

        /// <summary>
        /// Contient la liste des utilisateurs
        /// </summary>
        private Dictionary<string, (byte[], byte[])> _utilisateurs;

        public frmUtilisateur()
        {
            InitializeComponent();
        }

        private void frmUtilisateur_Load(object sender, EventArgs e)
        {

            ActiveControl = txtNomUtilisateur;
            InitialiserListeUtilisateur();
            btnSupprimer.Enabled = false;

        }

        private void btnNouveau_Click(object sender, EventArgs e)
        {
            //réinitialiser le formulaire
            InitialiserFormulaire();

        }

        /// <summary>
        /// Permet l'intialisation du formulaire
        /// </summary>
        private void InitialiserFormulaire()
        {
            txtNomUtilisateur.Clear();
            txtMotPasse.Clear();
        }

        /// <summary>
        /// Permet d'obtenir et d'afficher la liste des utilisateurs
        /// </summary>
        private void InitialiserListeUtilisateur()
        {
            lstUtilisateur.Items.Clear();

            if (_utilisateurs == null)
                _utilisateurs = Utilitaire.ObtenirUtilisateurs();

            foreach (string cle in _utilisateurs.Keys)
                lstUtilisateur.Items.Add(cle);


        }

        /// <summary>
        /// Permet l'ajout d'un nouvel utilsateur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAjouter_Click(object sender, EventArgs e)
        {
            try
            {

                if (ValiderUtilisateur())
                {
                    //vider les erreurs 
                    errorProvider.Clear();

                    if (_utilisateurs.ContainsKey(txtNomUtilisateur.Text))
                        errorProvider.SetError(txtNomUtilisateur, "L'utilisateur existe dejà");
                    else
                    {
                        // on ajoute de l'informtation dans le status bar pour faire patienter l'utilisateur.
                        lblStatusBar.Text = "Creation du compte d'utilisateur en cours... Veuillez patientez";

                        byte[] salt = Utilitaire.GenererSalt();
                        byte[] motDePasse = Utilitaire.HashMotDePasse(txtMotPasse.Text, salt);

                        _utilisateurs.Add(txtNomUtilisateur.Text.ToLower(), (salt, motDePasse));

                        lblStatusBar.Text = "";

                        MessageBox.Show("Utilisateur ajouté avec succès", "Ajout d'un utilisateur",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);

                        InitialiserListeUtilisateur();
                        InitialiserFormulaire();

                    }

                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

        /// <summary>
        /// Permet la suppression d'un utiliateur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            if (lstUtilisateur.SelectedIndex == -1)
                MessageBox.Show("Veuillez selectionnez un utilisateur", "Suppression d'un utilisateur",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                if (_utilisateurs.ContainsKey(lstUtilisateur.SelectedItem.ToString()))
                {
                    DialogResult reponse =
                        MessageBox.Show($"Desriez-vous supprimer l'utilisateur {lstUtilisateur.SelectedItem} ?",
                            "Suppression d'utilisateur", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (reponse == DialogResult.Yes)
                    {
                        _utilisateurs.Remove(lstUtilisateur.SelectedItem.ToString());
                        MessageBox.Show($"L'utilisateur {lstUtilisateur.SelectedItem} a été supprimé avec succès", "Suppression d'utilisateur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        InitialiserListeUtilisateur();
                    }

                }
                
            }

        }

        /// <summary>
        /// Efface le contenu du formulaire et ne permet la suppression de l'utilisateur sélectionné
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstUtilisateur_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitialiserFormulaire();
            btnSupprimer.Enabled = true;

        }

        /// <summary>
        /// Permet de valider les données saisie pour la création d'un utilisateur.
        /// </summary>
        /// <returns>True si les données sont valide, False sinon</returns>
        private bool ValiderUtilisateur()
        {
            //s'assurer de le vider à chauqe validation
            errorProvider.Clear();

            string messages = "";


            //Validation du nom
            if (string.IsNullOrWhiteSpace(txtNomUtilisateur.Text))
                errorProvider.SetError(txtNomUtilisateur, "Veuillez saisir un nom d'utilisateur");

            // On verifie si le nom respecte le nombre de caraceteres indiques
            else if (txtNomUtilisateur.Text.Replace(" ", "").Trim().Length < Joueur.NOM_NB_CARAC_MIN)
                errorProvider.SetError(txtNomUtilisateur,
                    $"Le nom doit être contenir au moins {JetonAuthentification.NB_MIN_CARACTERES_NOM} caractères");

            //Retirer tous les espaces du non de l'utilisateur.
            txtNomUtilisateur.Text = txtNomUtilisateur.Text.Replace(" ", "");


            //Validation mot de passe
            if (string.IsNullOrWhiteSpace(txtMotPasse.Text))
                errorProvider.SetError(txtMotPasse, "Veuillez saisir un mot de passe de l'utilisateur");

            // On verifie si le nom respecte le nombre de caraceteres indiques
            else if (txtMotPasse.Text.Replace(" ", "").Trim().Length < JetonAuthentification.NB_MIN_CARACTERES_MDP)
                errorProvider.SetError(txtMotPasse,
                    $"Le mot de passe doit être contenir au moins {JetonAuthentification.NB_MIN_CARACTERES_MDP} caractères");

            //Retirer tous les espaces du mot de passe.
            txtMotPasse.Text = txtMotPasse.Text.Replace(" ", "");


            // ICI JE SAIS PAS PK MAIS JARRIVE PAS A RECUPERER LES ERREURS C'EST BIZZARRE
            foreach (Control ctrl in errorProvider.ContainerControl.Controls)
            {
                string erreur = errorProvider.GetError(ctrl);

                if (!string.IsNullOrWhiteSpace(erreur))
                    messages += $"- {erreur}\n";
            }


            if (messages != "")
            {
                MessageBox.Show($"Veuillez corriger les erreurs suivantes : \n{messages}", "Utilisateurs",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;

        }


        /// <summary>
        /// Permet d'enregistrer la liste des utilisateurs dans le fichier  et ferme le formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEnregistrer_Click(object sender, EventArgs e)
        {

            try
            {
                Utilitaire.SauvegarderUtilisateurs(_utilisateurs);
                MessageBox.Show("Données enregistrées avec succès!", "Enregistrement des résultats", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Une erreur s'est produite: {ex.Message}", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// Annule les modifications et ferme le formulaire.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAnnuler_Click(object sender, EventArgs e)
        {

            try
            {
                DialogResult reponse =
                    MessageBox.Show("Désirez-vous enregistrer vos données avant de quitter ?", "Enregistrement des résultats",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (reponse == DialogResult.Yes)
                {
                    Utilitaire.SauvegarderUtilisateurs(_utilisateurs);
                    MessageBox.Show("Données enregistrées avec succès!", "Enregistrement des résultats", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }

                DialogResult = DialogResult.Cancel;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Une erreur s'est produite: {ex.Message}", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


    }
}
