﻿using System;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using _420_14C_FX.Classes;
using _420_14C_FX.Properties;


namespace _420_14C_FX
{
    public partial class frmUno : Form
    {
        private frmServeur _serveur;

        private Socket _clientSocket;

        byte[] receivedBuf = new byte[frmServeur.TAILLE_BUFFER];


        private const int MARGE = 30;
        private const int LARGEUR_CARTE = 105;
        private const int HAUTEUR_CARTE = 150;



        /// <summary>
        /// Partie en cours
        /// </summary>
        private Partie _partie;

        /// <summary>
        /// Joueur
        /// </summary>
        private Joueur _joueur;

        #region MÉTHODES CLIENT-SERVEUR

        private void ReceiveData(IAsyncResult ar)
        {
            Socket socket = (Socket)ar.AsyncState;
            int received = socket.EndReceive(ar);
            byte[] dataBuf = new byte[received];
            Array.Copy(receivedBuf, dataBuf, received);


            Object obj = Utilitaire.Deserialiser(dataBuf);

            // dans le cas ou c'est un jeton
            if (obj is JetonAuthentification jeton)
            {
                if (jeton.Authenfie)
                {
                    MiseAJourStatut($"La partie va bientôt commencer, le joueur {jeton.NomUtilisateur} à été authentifié avec succès. Veuillez patientez...");
                    _joueur = new Joueur(jeton.NomUtilisateur);

                }


                else
                {
                    socket.Close();
                    MiseAJourStatut("Le nom d’utilisateur ou mot de passe est invalide...");
                }

            }

            // Dans la cas d'une partie
            if (obj is Partie partie)
            {
                _partie = partie;
                MiseAjourInterface();
                MiseAJourStatut("");
            }

            //Si le client est connecté alors on initialise l'écoute pour la réception de données.
            if (socket.Connected)
                _clientSocket.BeginReceive(receivedBuf, 0, receivedBuf.Length, SocketFlags.None, new AsyncCallback(ReceiveData), _clientSocket);



            if (obj is string messageServeur)
            {
                MiseAJourStatut($"Message reçu du serveur : {messageServeur}");
            }

        }

        /// <summary>
        /// Permet de se connecter au serveur.
        /// </summary>
        /// <param name="pAddressIp">Adresse du seveur</param>
        /// <param name="pPort">Numéro du port</param>
        /// <remarks>5 tentatives de connexion sont exécutées avant d'abandonner.</remarks>
        private void Connecter(string pAddressIp, int pPort)
        {
            //Initialisation d'un nouveau socket
            _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            int nbEssais = 1;

            while (!_clientSocket.Connected && nbEssais <= 5)
            {
                try
                {

                    _clientSocket.Connect(IPAddress.Parse(pAddressIp), pPort);
                }
                catch (Exception)
                {
                    nbEssais++;

                }
            }

            if (!_clientSocket.Connected)
            {
                MessageBox.Show(
                    $"Impossible de se connecter au serveur à l'adresse {pAddressIp} sur le port {pPort}. Veuillez vous assurez que vos informations de connexion son correctes.");
            }



        }

        /// <summary>
        /// Permet l'envoi d'un objet au serveur 
        /// </summary>
        /// <param name="obj">Objet à sérialisé et à envoyer au serveur</param>
        private void EnvoyerDonnees(Object obj)
        {
            if (_clientSocket.Connected)
            {

                //Sérialisation de l'objet à envoyer
                byte[] data = Utilitaire.Serialiser(obj);

                //Envoi de l'objet au serveur.
                _clientSocket.Send(data);
            }
            else
            {
                MessageBox.Show("Impossible d'envoyer les données au serveur. Vous n'êtes pas connecté!",
                    "Envoi des données");
            }
        }

        #endregion


        public frmUno()
        {
            InitializeComponent();

        }


        private void frmUno_Load(object sender, EventArgs e)
        {
            //Ajustement du panel du jeu selon la largeur de la fenetre.
            pnlTable.Width = Width - 25;
            pnlTable.Height = Height - 100;

            //Affichage de l'adresse Ip de l'utiliateur
            Text = $"Uno ({Utilitaire.ObtenirAdresseIpLocale()})";


            lblStatut.Text = "Vous n'êtes pas connecté.";


        }

        /// <summary>
        /// Permet la mise à jour du contrôle lblStatut par un thread différent du UI.
        /// </summary>
        /// <param name="pMessage">Message à afficher dans le label.</param>
        private void MiseAJourStatut(string pMessage)
        {

            statutStrip.Invoke((Action)(() =>
           {
               lblStatut.Text = pMessage;
           }));
        }

        /// <summary>
        /// Permet la mise à jour de l'interface du jeu après la réception de l'état de la partie venant du serveur.
        /// Le nom est les cartes des joueurs sont dessinés.
        /// Le pioche est dessinée.
        /// La défausse est dessinée.
        /// Le bouton Uno est dessiné s'il reste 2 cartes au joueur à jouer.
        /// </summary>
        /// <remarks>On vérifie également s'il y a gagnant.</remarks>
        private void MiseAjourInterface()
        {

            pnlTable.Invoke((Action)(() =>
            {
                pnlTable.Controls.Clear();

                if (_partie != null && _partie.Joueurs.TrouverGagnant() == null)
                {
                    // Le premier joueur dans la liste.
                    Noeud joueur = _partie.Joueurs.Debut;

                    for (int i = 0; i < _partie.Joueurs.Taile; i++)
                    {
                        DessinerMainJoueur(joueur.Valeur, i + 1);

                        if (joueur.Valeur.Main.Count != 2)
                            joueur.Valeur.Uno = true;

                        else
                        {
                            joueur.Valeur.Uno = false;
                            DessinerBoutonUno();
                        }

                        joueur.Valeur.Main.Sort();
                        joueur = joueur.Suivant;

                    }
                }

                else if (_partie.Joueurs.TrouverGagnant() != null)
                    MessageBox.Show($"{_joueur.Nom} a gagné!","Fin de jeu", MessageBoxButtons.OK);

                DessinerPioche();
                DessinerDefausse();


            }));
        }

        /// <summary>
        /// Permet de dessiner la pioche
        /// </summary>
        private void DessinerPioche()
        {
            if (_partie.Pioche.NbCartes > 0)
            {
                //Création d'une nouvelle image
                PictureBox pboPioche = new PictureBox();
                pboPioche.Name = "pboPioche";
                pboPioche.Width = frmUno.LARGEUR_CARTE;
                pboPioche.Height = frmUno.HAUTEUR_CARTE;
                pboPioche.SizeMode = PictureBoxSizeMode.Zoom;
                pboPioche.Image = Resources.CarteDessus;
                pboPioche.AllowDrop = true;

                pboPioche.Location = new Point(Width / 2 - frmUno.LARGEUR_CARTE - 20, Height / 2 - frmUno.HAUTEUR_CARTE / 2);

                pboPioche.MouseMove += new MouseEventHandler(PictureBox_MouseMove);
                pboPioche.DragEnter += new DragEventHandler(PictureBox_DragEnter);
                pboPioche.DragDrop += new DragEventHandler(PictureBox_DragDrop);

                pnlTable.Controls.Add(pboPioche);
            }

        }

        /// <summary>
        /// Permet de dessiner la défausse.
        /// </summary>
        private void DessinerDefausse()
        {
            if (_partie.Defausse.Count > 0)
            {
                //Création d'une nouvelle image
                PictureBox pboDefausse = new PictureBox();
                pboDefausse.Name = "pboDefausse";
                pboDefausse.Width = frmUno.LARGEUR_CARTE;
                pboDefausse.Height = frmUno.HAUTEUR_CARTE;
                pboDefausse.SizeMode = PictureBoxSizeMode.Zoom;
                pboDefausse.AllowDrop = true;

                pboDefausse.Location = new Point(Width / 2 + frmUno.LARGEUR_CARTE, Height / 2 - frmUno.HAUTEUR_CARTE / 2);

                //pbDefausse.MouseMove += new MouseEventHandler(PictureBox_MouseMove);
                pboDefausse.DragEnter += new DragEventHandler(PictureBox_DragEnter);
                pboDefausse.DragDrop += new DragEventHandler(PictureBox_DragDrop);


                Carte carteDefausse = _partie.Defausse.Peek();

                pboDefausse.Image = ObtenirImage(carteDefausse);

                pnlTable.Controls.Add(pboDefausse);
            }

        }


        /// <summary>
        /// Permet d'obtenir l'image d'une carte à partir de son nom.
        /// </summary>
        /// <param name="pCarte">Carte</param>
        /// <returns>L'image correspondant a la carte dans les ressources.</returns>
        private Image ObtenirImage(Carte pCarte)
        {
            string nomRessource = pCarte.Valeur.ToString() + pCarte.Couleur;

            return (Image)Resources.ResourceManager.GetObject(nomRessource);
        }

        /// <summary>
        /// Permet de dessiner le bouton Uno!
        /// </summary>
        /// <remarks>Celui-ci est dessiner seulement s'il reste 2 cartes à jour par le joueur. Le joueur ne peut pas jouer son avant dernière carte s'il n'a pas cliqué sur ce bouton.</remarks>
        private void DessinerBoutonUno()
        {
            //Création d'une nouvelle image
            PictureBox pboUno = new PictureBox();
            pboUno.Name = "pboUno";
            pboUno.Width = frmUno.LARGEUR_CARTE;
            pboUno.Height = frmUno.HAUTEUR_CARTE;
            pboUno.SizeMode = PictureBoxSizeMode.Zoom;
            pboUno.Image = Resources.uno;
            pboUno.Location = new Point(Width / 2, Height / 2 - frmUno.HAUTEUR_CARTE / 2);
            pboUno.Click += new EventHandler(pictureBoxUno_Click);

            pnlTable.Controls.Add(pboUno);

        }

        /// <summary>
        /// Permet de dessiner le nom du joueur et ses cartes.
        /// </summary>
        /// <param name="pJoueur">Joueur à dessiner</param>
        /// <param name="pNoJoueur">Numéro du joueur. Nécessaire pour déterminer la position du jour sur l'interface.</param>
        private void DessinerMainJoueur(Joueur pJoueur, int pNoJoueur)
        {

            int posXCarte = 0, posYCarte;
            int posXLabel = 0, posYLabel = 0;


            //Création du label du joueur
            Label lblNomJoueur = new Label();
            lblNomJoueur.Text = pJoueur.Nom;
            lblNomJoueur.Font = new Font("Cooper Black", 12);

            //Le nom du joueur courant est en bleu
            if (pJoueur == _partie.JoueurCourant.Valeur)
                lblNomJoueur.ForeColor = Color.Blue;
            else
                lblNomJoueur.ForeColor = Color.Black;


            //On détermine la position graphique des éléments du joueur.

            switch (pNoJoueur)
            {
                case 1:
                    //Nom du joueur
                    posXLabel = frmUno.MARGE;
                    posYLabel = frmUno.MARGE;

                    //Cartes
                    posXCarte = posXLabel;

                    break;
                case 2:
                    //Nom du joueur
                    posXLabel = Width - frmUno.MARGE - lblNomJoueur.Width;
                    posYLabel = frmUno.MARGE;

                    //Cartes
                    posXCarte = Width - frmUno.MARGE - frmUno.LARGEUR_CARTE;

                    break;
                case 3:
                    //Nom du joueur
                    posXLabel = Width - frmUno.MARGE - lblNomJoueur.Width;
                    posYLabel = Height - frmUno.MARGE - frmUno.HAUTEUR_CARTE - lblNomJoueur.Height - 5;

                    //Cartes
                    posXCarte = Width - frmUno.MARGE - frmUno.LARGEUR_CARTE;

                    break;
                case 4:

                    //Nom du joueur
                    posXLabel = frmUno.MARGE;
                    posYLabel = Height - frmUno.MARGE - frmUno.HAUTEUR_CARTE - lblNomJoueur.Height - 5;

                    //Carte
                    posXCarte = posXLabel;

                    break;
            }

            //Position des cartes sous le nom du joueur
            posYCarte = posYLabel + lblNomJoueur.Height + 5;

            //Position du nom du joueur
            lblNomJoueur.Location = new Point(posXLabel, posYLabel);

            //Ajout un nom du joueur
            pnlTable.Controls.Add(lblNomJoueur);

            //Création des cartes du joueur
            foreach (Carte carte in pJoueur.Main)
            {
                //Création d'une nouvelle image
                PictureBox pbCarte = new PictureBox();

                //S'il s'agit d'un adversaire, alors on ne montre que le dessus des cartes
                if (pJoueur.Nom != _joueur.Nom)
                    pbCarte.Image = Resources.CarteDessus;
                else
                    pbCarte.Image = ObtenirImage(carte);
                //pbCarte.Image = carte.Image;

                pbCarte.Width = frmUno.LARGEUR_CARTE;
                pbCarte.Height = frmUno.HAUTEUR_CARTE;
                pbCarte.SizeMode = PictureBoxSizeMode.Zoom;
                pbCarte.AllowDrop = true;

                //S'il s'agit du joueur alors on permet le déplacement des carte.
                if (pJoueur == _partie.JoueurCourant.Valeur)
                {
                    pbCarte.MouseMove += new MouseEventHandler(PictureBox_MouseMove);
                    pbCarte.DragEnter += new DragEventHandler(PictureBox_DragEnter);
                    pbCarte.DragDrop += new DragEventHandler(PictureBox_DragDrop);
                }




                //Positionnement de la carte
                pbCarte.Location = new Point(posXCarte, posYCarte);

                //Ajout d'un tuple contenant le joueur et la carte pour jouer la carte.
                pbCarte.Tag = (pJoueur, carte);

                //Espace pour la juxtaposition des cartes
                int diviseur = 4;

                //S'il s'agit du joueur courant alors on espace d'avantage les cartes
                if (pJoueur == _partie.JoueurCourant.Valeur)
                    diviseur = 3;



                //Il s'agit du joueur 2 ou 3, on ajoute les cartes vers le centre du formulaire.
                if (pNoJoueur == 1 || pNoJoueur == 4)
                    posXCarte += pbCarte.Width / diviseur;
                else
                    posXCarte -= pbCarte.Width / diviseur;

                //Ajout de la carte
                pnlTable.Controls.Add(pbCarte);
                pbCarte.BringToFront();

            }

            pnlTable.Refresh();

        }

        /// <summary>
        /// Événement généré lors du déplacement d'une carte.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            //Si le bouton de gauche de la souris est enfoncé?
            if (e.Button == MouseButtons.Left)
            {
                PictureBox pb = (PictureBox)sender;

                //Démarrage du DragDrop
                pb.DoDragDrop(pb, DragDropEffects.Move);

            }
        }

        /// <summary>
        /// Événement généré lorsqu'un carte commence à ce déplacer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PictureBox_DragEnter(object sender, DragEventArgs e)
        {
            //Modification du curseur lors du déplacement
            e.Effect = DragDropEffects.Move;

        }

        /// <summary>
        /// Événement générée lorsque la carte est déposée
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PictureBox_DragDrop(object sender, DragEventArgs e)
        {

            //On obtient la cible 
            PictureBox target = (PictureBox)sender;
            if (e.Data.GetDataPresent(typeof(PictureBox)))
            {
                //On obtient la source
                PictureBox source = (PictureBox)e.Data.GetData(typeof(PictureBox));
                if (source != target)
                {
                    //Démarrage du timer pour l'action à effectuer suite au Drop.
                    //Requis pour ne pas que la fenêtre gèle lors de l'affichage de messagebox 
                    //ou de formulaire.
                    tmrDragDrop.Tag = source;
                    tmrDragDrop.Start();

                }
            }


        }

        /// <summary>
        /// Événement du timer pour enclancher une action suite à un déplacement d'une carte.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tmrDragDrop_Tick(object sender, EventArgs e)
        {

            //Arrêt du timer
            tmrDragDrop.Stop();

            //On obtien la source (picturebox) qui a été glissé et qui a démarré le timer
            PictureBox pictureBox = (PictureBox)tmrDragDrop.Tag;


            try
            {

                if (pictureBox.Tag == null)
                    _partie.PigerCartes(_partie.JoueurCourant.Valeur, 1);

                else
                {
                    (Joueur, Carte) joueur = ((Joueur, Carte))pictureBox.Tag;
                    _partie.JouerCarte(joueur.Item1, joueur.Item2);

                    Carte carteDeJeu;

                    if (joueur.Item2.Valeur == Valeur.Joker || joueur.Item2.Valeur == Valeur.Plus4)
                    {
                        carteDeJeu = new Carte(DemanderCouleur(), joueur.Item2.Valeur, joueur.Item2.Points);
                        _partie.Defausse.Push(carteDeJeu);
                    }


                }

             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }


            //Envoi de l'état de la partie au serveur.
            EnvoyerDonnees(_partie);
            MiseAJourStatut("Envoi de l'état de la partie au serveur ...");


        }

        /// <summary>
        /// Permet au joueur de sélectionner une couleur dans le formulaire frmSelectionCourleur
        /// </summary>
        /// <returns>La Courleur sélectionnée par le joueur</returns>
        private Couleur DemanderCouleur()
        {

            frmSelectionCouleur frm = new frmSelectionCouleur();
            frm.ShowDialog();
            Couleur choix = frm.CouleurSelectionnee;
            frm.Dispose();

            return choix;
        }


        /// <summary>
        /// Permet l'ouverture de la fenêtre de connexion au serveur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuConnexionServeur_Click(object sender, EventArgs e)
        {
            if (_serveur == null || _serveur.IsDisposed)
                _serveur = new frmServeur();

            _serveur.Show();


        }

        /// <summary>
        /// Permet l'ouverture de la fenêtre d'authentification.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuConnexionClient_Click(object sender, EventArgs e)
        {


            frmAuthentification frmAuth = new frmAuthentification();
            DialogResult reponse = frmAuth.ShowDialog();

            switch (reponse)
            {
                case DialogResult.OK:

                    JetonAuthentification jAuth = new JetonAuthentification(frmAuth.NomUtilisateur, frmAuth.MotPasse);
                    //Connection au serveur
                    Connecter(frmAuth.Serveur, frmAuth.Port);

                    if (_clientSocket.Connected)
                    {
                        //Initialisation de l'écoute de réception des données
                        _clientSocket.BeginReceive(receivedBuf, 0, receivedBuf.Length, SocketFlags.None, new AsyncCallback(ReceiveData), _clientSocket);

                        EnvoyerDonnees(jAuth);

                        //lblStatut.Text = "Authentification en cours. Veuillez patienter ...";
                        MiseAJourStatut("Authentification en cours. Veuillez patienter ...");
                    }

                    break;
                case DialogResult.Cancel:
                    frmAuth.Close();
                    break;
                default:
                    break;
            }



        }


        /// <summary>
        /// Permet l'ouverture de la fenêtre de gestion des utilisateurs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuUtilisateurs_Click(object sender, EventArgs e)
        {
            frmUtilisateur frmUtilisateur = new frmUtilisateur();

            frmUtilisateur.ShowDialog();


        }

        /// <summary>
        /// Permet de redessiner le jeu lorsqu'il y a un redimensionnement de l'écra..
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmUno_Resize(object sender, EventArgs e)
        {
            //On vérifie si le formulaire est affiché.
            if (Visible)
            {
                pnlTable.Width = Width - 25;
                pnlTable.Height = Height - 100;
                MiseAjourInterface();
            }

        }

        /// <summary>
        /// Événement exécuté lors du clique sur l'image Uno.
        /// Permet d'indiqué que le joueur a crié Uno!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBoxUno_Click(object sender, EventArgs e)
        {
            _partie.JoueurCourant.Valeur.Uno = true;
            pnlTable.Controls.Remove((PictureBox)sender);

        }
    }
}
